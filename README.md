# CNN ARCHITECTURES FOR SMALL DATASETS

* 3 possible labels - Bart, Homer or Moe
* 500 Train images for each class
* 500 Validation images for each class
* No data augmentation
* Tricky image to classify after training the model to test it

# 1st_model

3 blocks of convolution with the same pattern: convolution, maxpooling and dropout.

64 filters, 32 filters and 32 filters.

After flatten, one single dense layer, 500 units followed by 50% dropout

0.0004 learning rate

    model = models.Sequential()

    model.add(layers.Conv2D(64, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Conv2D(32, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Conv2D(32, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Flatten())
    model.add(layers.Dense(500, activation="relu"))
    model.add(layers.Dropout(0.50))

    model.add(layers.Dense(3, activation="softmax"))


    model.compile(loss="categorical_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=48, epochs=100, validation_data=val_generator, validation_steps=24)

![alt text](photos/1st_model.png)
![alt text](photos/1st_model_pred.png)

<h2>Conclusion</h2>

Despite all the dropouts, the model overfitted quite a bit, and very fast too. Train accuracy got a 97% score, while validation didn't even get 80%, stopping at a 76%. The model's prediction of bart's picture missed very confidently, predicted homer with 79% confidence.


# 2nd_model

In the second model i decided to use less filters, still in a decreasing architecture. Kept the three blocks of convolutions, and the same pattern; convolutional layer, maxpooling and 50% dropout. I decreased the learning rate to 0.00001 and increased the epochs to 500 instead of 100. The dense block of the neural network is still the exact same.

    model = models.Sequential()
    model.add(layers.Conv2D(32, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Conv2D(16, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Conv2D(8, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Flatten())
    model.add(layers.Dense(500, activation="relu"))
    model.add(layers.Dropout(0.50))

    model.add(layers.Dense(3, activation="softmax"))
    model.summary()

    model.compile(loss="categorical_crossentropy", optimizer=optimizers.RMSprop(lr=1e-5), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=48, epochs=500, validation_data=val_generator, validation_steps=24)

![alt text](photos/2nd_model.png)
![alt text](photos/2nd_model_pred.png)

<h2>Conclusion</h2>

The performance graphic has an interesting shape. Its very unstable; The model spent a lot of time around 60% on validation accuracy, while the train accuracy slowly got better, eventually kind of overfitting, with the validation accuracy 10% worse. The model predicted the picture wrong, but with a low confidence.

# 3rd_model

This time tried two convolution blocks, still using 50% dropouts and one dense block with the same amount of dropout. Instead of using decreasing sizes for the filters, i've used increasing sizes for the convolutional layers.

    model = models.Sequential()
    model.add(layers.Conv2D(32, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Flatten())
    model.add(layers.Dense(500, activation="relu"))
    model.add(layers.Dropout(0.50))

    model.add(layers.Dense(3, activation="softmax"))
    model.summary()

    model.compile(loss="categorical_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=48, epochs=100, validation_data=val_generator, validation_steps=24)

![alt text](photos/3rd_model.png)
![alt text](photos/3rd_model_pred.png)

<h2>Conclusion</h2>

This architecture overfitted the most so far, and very fast as well. Very soon the validation accuracy started differing from the train accuracy, which kept increasing until as high as 100%, while the validation just kept a relatively steady performance, far bellow. Until now i've used shallow models, as more compact neural networks theoretically would not adapt to data so fast, but none of our models was capable of correctly labeling bart's picture, so i decided to change the architecture just a bit.

# 4th_model

Keeping things fairly simple, i've tried using two dense layers with double the units, 1000, as well as three convolutional layers, decreasingly, with 256, 128 and 64 filters. Also used $`lr = 1-^5`$ & $`decay = 1-^7`$ which required more epochs to train.

    model = models.Sequential()
    model.add(layers.Conv2D(256, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.50))

    model.add(layers.Flatten())
    model.add(layers.Dense(1000, activation="relu"))
    model.add(layers.Dropout(0.50))
    model.add(layers.Dense(1000, activation="relu"))
    model.add(layers.Dropout(0.50))

    model.add(layers.Dense(3, activation="softmax"))
    model.summary()

    model.compile(loss="categorical_crossentropy", optimizer=optimizers.RMSprop(lr=1e-5, decay=1e-7), metrics=["acc"])

![alt text](photos/4th_model.png)
![alt text](photos/4th_pred.png)

<h2>Conclusion</h2>

As we can see in the graphic, decay slowly stabilized train accuracy's performance, but the model started overfitting before the 50th epoch. Validation's green line shows an interesting, unstable performance. Even tho it started doing pretty well, things went downhill fast. This model predicted the picture wrong (as well as every single model so far) with a confidence of 86%. In comparison to the other architecures, this one is the "second best" so far, with 75% validation accuracy, it falls short on beatting the first model which leads on val_acc with 76%.